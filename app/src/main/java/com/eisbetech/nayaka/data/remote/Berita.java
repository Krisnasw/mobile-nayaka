package com.eisbetech.nayaka.data.remote;

public class Berita {
    String judul;
    String konten;
    String gambar;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getKonten() {
        return konten;
    }

    public void setKonten(String konten) {
        this.konten = konten;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public Berita(){}

    public Berita(String judul, String konten, String gambar) {
        this.judul = judul;
        this.konten = konten;
        this.gambar = gambar;
    }
}
