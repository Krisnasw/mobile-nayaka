package com.eisbetech.nayaka.data.remote;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private String price;
    @SerializedName("author")
    private String author;
    @SerializedName("quantity")
    private Integer qty;
    @SerializedName("sold")
    private Integer sold;
    @SerializedName("kategori")
    private String kategori;
    @SerializedName("berat")
    private Integer berat;
    @SerializedName("kondisi")
    private String kondisi;
    @SerializedName("images")
    private String images;

    public Product(String title, String price, String author, Integer qty, Integer sold, String kategori, Integer berat, String kondisi, String images) {
        this.title = title;
        this.price = price;
        this.author = author;
        this.qty = qty;
        this.sold = sold;
        this.kategori = kategori;
        this.berat = berat;
        this.kondisi = kondisi;
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}