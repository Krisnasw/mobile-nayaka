package com.eisbetech.nayaka.data.remote;

public class Forum {

    String judul;
    String sub;
    String gambar;

    public Forum(String judul, String sub, String gambar) {
        this.judul = judul;
        this.sub = sub;
        this.gambar = gambar;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
