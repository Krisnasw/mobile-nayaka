package com.eisbetech.nayaka.data.remote;

public class ThreadItem {
    String name;
    long timestamp;
    String content;
    String avatar;

    public ThreadItem(String name, long timestamp, String content, String avatar) {
        this.name = name;
        this.timestamp = timestamp;
        this.content = content;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
