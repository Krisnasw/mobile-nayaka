package com.eisbetech.nayaka;

import android.app.Application;
import android.content.ContextWrapper;
import android.content.Intent;

import com.pixplicity.easyprefs.library.Prefs;

public class EisbetechApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

}
