package com.eisbetech.nayaka.ui.home;

import android.Manifest;
import android.arch.lifecycle.ReportFragment;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.ui.account.AccountFragment;
import com.eisbetech.nayaka.ui.forum.ForumFragment;
import com.eisbetech.nayaka.ui.login.LoginActivity;
import com.eisbetech.nayaka.ui.news.NewsFragment;
import com.eisbetech.nayaka.ui.notification.NotificationActivity;
import com.eisbetech.nayaka.utils.AppConfig;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.pixplicity.easyprefs.library.Prefs;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_container)
    FrameLayout frameLayout;
    @BindView(R.id.bottombar)
    BottomBar bottomBar;
    @BindView(R.id.search_view)
    MaterialSearchView materialSearchView;
    @BindView(R.id.container)
    RelativeLayout layout;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_CONTACTS
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                Toast.makeText(HomeActivity.this, "Granted!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();

        for (int i = 0; i < bottomBar.getTabCount(); i++) {
            bottomBar.getTabAtPosition(i).setGravity(Gravity.CENTER_VERTICAL);
        }

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                Fragment fragment = null;
                switch (tabId) {
                    case R.id.home:
                        attachFragment(new HomeFragment());
                        break;
                    case R.id.berita:
                        attachFragment(new NewsFragment());
                        break;
                    case R.id.forum:
                        attachFragment(new ForumFragment());
                        break;
                    case R.id.akun:
                        attachFragment(new AccountFragment());
                        break;
                    default:
                        attachFragment(new HomeFragment());
                        break;
                }
            }
        });

        if (materialSearchView.isSearchOpen()) {
            materialSearchView.setHint("Masukkan Pencarian");
            materialSearchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
            materialSearchView.setVoiceSearch(true);
        }
    }

    public void attachFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        trans.replace(R.id.fragment_container, fragment);
        trans.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mn_notification:
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                break;
            case R.id.mn_search:
                materialSearchView.showSearch(true);
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        if (materialSearchView.isSearchOpen()) {
            materialSearchView.closeSearch();
        } else {
            Snackbar snackbar = Snackbar
                    .make(layout, "Apakah Anda Yakin Akan Keluar ?", Snackbar.LENGTH_LONG)
                    .setAction("YA", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                            Prefs.clear();
                            finish();
                        }
                    });

            snackbar.show();
        }
    }

}
