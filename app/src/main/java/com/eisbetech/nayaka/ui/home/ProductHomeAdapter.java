package com.eisbetech.nayaka.ui.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.Berita;
import com.eisbetech.nayaka.data.remote.Product;
import com.eisbetech.nayaka.utils.Constant;

import java.util.List;

public class ProductHomeAdapter extends RecyclerView.Adapter<ProductHomeAdapter.holder> {
    Context context;
    List<Product> data;

    public ProductHomeAdapter(Context context, List<Product> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ProductHomeAdapter.holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_home_berita, parent, false);

        return new ProductHomeAdapter.holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHomeAdapter.holder holder, int position) {
        Product product = data.get(position);
        Glide.with(context.getApplicationContext()).load(product.getImages()).into(holder.mCoverVideoItem);
        holder.mJudulBerita.setText(product.getTitle());
        holder.mSubBerita.setText(Constant.toRupiah(Long.parseLong(product.getPrice())));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {

        ImageView mCoverVideoItem;
        TextView mJudulBerita;
        TextView mSubBerita;

        public holder(View itemView) {
            super(itemView);
            mCoverVideoItem = itemView.findViewById(R.id.cover_video_item);
            mJudulBerita = itemView.findViewById(R.id.judul_berita);
            mSubBerita = itemView.findViewById(R.id.sub_berita);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
