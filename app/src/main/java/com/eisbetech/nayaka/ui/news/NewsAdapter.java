package com.eisbetech.nayaka.ui.news;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.Berita;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.holder> {
    Context context;
    List<Berita> data;

    public NewsAdapter(Context context, List<Berita> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        Berita berita = data.get(position);
        Glide.with(context).load(berita.getGambar()).into(holder.imageView);
        holder.title.setText(berita.getJudul());
        holder.subTitle.setText(berita.getKonten());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_news_item)
        ImageView imageView;
        @BindView(R.id.title_berita_item)
        TextView title;
        @BindView(R.id.sub_news_item)
        TextView subTitle;

        public holder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(context, NewsDetailActivity.class);
//                    context.startActivity(intent);
                }
            });
        }
    }
}
