package com.eisbetech.nayaka.ui.home;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.Berita;

import java.util.List;

public class NewsHomeAdapter extends RecyclerView.Adapter<NewsHomeAdapter.holder> {
    Context context;
    List<Berita> data;

    public NewsHomeAdapter(Context context, List<Berita> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_home_berita, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        Berita berita = data.get(position);
        Glide.with(context.getApplicationContext()).load(berita.getGambar()).into(holder.mCoverVideoItem);
        holder.mJudulBerita.setText(berita.getJudul());
        holder.mSubBerita.setText(berita.getKonten());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {

        ImageView mCoverVideoItem;
        TextView mJudulBerita;
        TextView mSubBerita;

        public holder(View itemView) {
            super(itemView);
            mCoverVideoItem = itemView.findViewById(R.id.cover_video_item);
            mJudulBerita = itemView.findViewById(R.id.judul_berita);
            mSubBerita = itemView.findViewById(R.id.sub_berita);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(context, NewsDetailActivity.class);
//                    context.startActivity(intent);
                }
            });
        }
    }
}
