package com.eisbetech.nayaka.ui.forum;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.Forum;

import java.util.List;

public class ForumAdapter extends RecyclerView.Adapter<ForumAdapter.holder>{
    Context context;
    List<Forum> data;

    public ForumAdapter(Context context, List<Forum> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_forum_content, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        Forum forum = data.get(position);
        Glide.with(context.getApplicationContext()).load(forum.getGambar()).into(holder.mImageBannerOffer);

        holder.mTittleOffer.setText(forum.getJudul());
        holder.mSubOffer.setText(forum.getSub());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {
        ImageView mImageBannerOffer;
        TextView mTittleOffer;
        TextView mSubOffer;

        public holder(View itemView) {
            super(itemView);
            mImageBannerOffer = (ImageView) itemView.findViewById(R.id.image_banner_offer);
            mTittleOffer = (TextView) itemView.findViewById(R.id.tittle_offer);
            mSubOffer = (TextView) itemView.findViewById(R.id.sub_offer);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, ForumDetailActivity.class));
                }
            });
        }
    }
}
