package com.eisbetech.nayaka.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.local.GridModel;
import com.eisbetech.nayaka.data.remote.Berita;
import com.eisbetech.nayaka.data.remote.Product;
import com.eisbetech.nayaka.ui.banner.BannerAdapter;
import com.eisbetech.nayaka.ui.product.ProductActivity;
import com.eisbetech.nayaka.ui.product.ProductDetailActivity;
import com.eisbetech.nayaka.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.relex.circleindicator.CircleIndicator;

public class HomeFragment extends Fragment {

    String title[] = {"Pulsa", "Paket data", "Token Listrik", "Tagihan Listrik", "Voucher Game",
            "Asuransi", "Televisi", "PDAM", "Internet", "Multifinance", "Kereta", "Pesawat"};

    Integer images[] = {R.drawable.ic_pulsa, R.drawable.ic_paket_data, R.drawable.ic_token_listrik, R.drawable.ic_tagihan_listrik,
            R.drawable.ic_voc_game, R.drawable.ic_insurance, R.drawable.ic_television,
            R.drawable.ic_pdam, R.drawable.ic_internet, R.drawable.ic_multifinance, R.drawable.ic_train, R.drawable.ic_flight};

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.list_berita_home)
    RecyclerView mListBeritaHome;
    @BindView(R.id.android_gridview_example)
    LinearLayout mAndroidGridviewExample;
    @BindView(R.id.all_trend)
    TextView mAllTrend;
    @BindView(R.id.list_trend)
    RecyclerView mListTrend;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;
    @BindView(R.id.iv_avatar_home)
    ImageView mIvAvatarHome;
    @BindView(R.id.tv_name_profil)
    TextView mTvNameProfil;
    @BindView(R.id.tv_balance)
    TextView mTvBalance;
    @BindView(R.id.button_topup)
    Button mButtonTopup;

    private BannerAdapter adapterBanner;
    private List<String> dataBanner;

    private List<Berita> dataBerita;
    private NewsHomeAdapter madapterNews;

    private GridLayoutManager gridManager;
    private List<GridModel> dataGrid;
    private AdapterHome adapterHome;

    private List<Product> products;
    private ProductHomeAdapter productHomeAdapter;

    private Unbinder unbinder;

    public HomeFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);

        dataBanner = new ArrayList<>();
        dataBanner.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNOQwIOy8JOn1yuIoW_B6JXjmSMCUOiSdrkM7fy9WzhXxHPpAA");
        dataBanner.add("https://winpoin.com/wp-content/uploads/2015/08/panduantokoonline_0.jpeg");
        dataBanner.add("http://www.velocityconsultancy.com/wp-content/uploads/2017/12/ecommerce.jpg");

        adapterBanner = new BannerAdapter(getActivity(), dataBanner);

        mViewPager.setAdapter(adapterBanner);
        mIndicator.setViewPager(mViewPager);

        adapterBanner.registerDataSetObserver(mIndicator.getDataSetObserver());

        gridManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerView.setLayoutManager(gridManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        dataGrid = new ArrayList<>();

        for (int i = 0; i < title.length; i++) {
            GridModel gridModel = new GridModel(title[i], images[i]);
            dataGrid.add(gridModel);
        }

        adapterHome = new AdapterHome(getActivity(), dataGrid);
        mRecyclerView.setAdapter(adapterHome);

        Berita berita1 = new Berita("Tips Mengatur Keuangan Rumah Tangga yang Bergaji 5 Juta", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn1-production-images-kly.akamaized.net/yhf54xmfDVI_9s2Wv1PPrbl2Eqc=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2285188/original/080488700_1531990071-Tips_Mengatur_Keuangan.jpg");
        Berita berita2 = new Berita("Re Clean, Solusi Terbaru Mencuci Tanpa Kucek dan Menggiling Pakaian", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn1-production-images-kly.akamaized.net/08Zhak98fptv_VP4Y1A8nZtIORY=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2243280/original/026400700_1528430560-Mengucek.jpg");
        Berita berita3 = new Berita("Pisau Bisa Jadi Penentu Makanan Lezat, Kok Bisa?", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn1-production-images-kly.akamaized.net/kbovJbVX7JkTj7g7pg_9Ju7l8M8=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2044440/original/039698500_1522472186-tokped.jpg");
        Berita berita4 = new Berita("Kesal Diberi Review Jelek, Pemilik Toko Online Hajar Pembeli", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn0-production-images-kly.akamaized.net/_2sJQLXp3NLvbZlsj0ntl5hZcFc=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/1596570/original/088231500_1495007197-3.jpg");

        dataBerita = new ArrayList<>();
        dataBerita.add(berita1);
        dataBerita.add(berita2);
        dataBerita.add(berita3);
        dataBerita.add(berita4);

        madapterNews = new NewsHomeAdapter(getActivity(), dataBerita);
        mListBeritaHome.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mListBeritaHome.addItemDecoration(new Constant.LeftItemDecotaion(15));
        mListBeritaHome.setAdapter(madapterNews);
        mListBeritaHome.setNestedScrollingEnabled(false);

        Product product1 = new Product("Gamis Syari Pesta Coklat Milo", "100000", "Nayaka", 10, 5, "Pakaian Wanita", 10, "Baru", "https://d3ife8wk53juxx.cloudfront.net/media/product-mp/38401/resize_MM.jpg");
        Product product2 = new Product("Grosir Dress Wanita", "50000", "Nayaka", 10, 5, "Pakaian Wanita", 10, "Baru", "http://www.kulaku.com/image/cache/data/BTN-GAMIS-A-bhn-prem-crepe-@99rb-seri-6pcs-500x500.jpg");
        Product product3 = new Product("Toy Machine Striped Crop Tee", "80000", "Nayaka", 10, 5, "Pakaian Wanita", 10, "Baru", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTRGqTduAhd5SWzVIDkScM73cjcrsiEHymWGqRV8JCVwGWBssKyiw");
        Product product4 = new Product("Erigo Sukajan", "150000", "Nayaka", 10, 5, "Pakaian Wanita", 10, "Baru", "https://pbs.twimg.com/media/DO5p7mkUMAE-cq6.jpg");

        products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);

        productHomeAdapter = new ProductHomeAdapter(getActivity(), products);
        mListTrend.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mListTrend.addItemDecoration(new Constant.LeftItemDecotaion(15));
        mListTrend.setAdapter(productHomeAdapter);
        mListTrend.setNestedScrollingEnabled(false);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_topup, R.id.recyclerView, R.id.list_berita_home, R.id.all_trend, R.id.list_trend, R.id.android_gridview_example})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.button_topup:
                break;
            case R.id.recyclerView:
                break;
            case R.id.list_berita_home:
                break;
            case R.id.all_trend:
                startActivity(new Intent(getContext(), ProductActivity.class));
                break;
            case R.id.list_trend:
                break;
            case R.id.android_gridview_example:
                break;
        }
    }
}
