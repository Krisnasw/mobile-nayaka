package com.eisbetech.nayaka.ui.banner;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.eisbetech.nayaka.R;

import java.util.List;

/**
 * Created by Dany on 05/02/2018.
 */

public class BannerAdapter extends PagerAdapter {
    List<String> data;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public BannerAdapter(Context context, List<String> data) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_banner, container, false);

        ImageView imageView = itemView.findViewById(R.id.item_pager_img);
        try {
            Glide.with(mContext)
                    .load(data.get(position))
                    .into(imageView);
        } catch (Exception e) {}

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}