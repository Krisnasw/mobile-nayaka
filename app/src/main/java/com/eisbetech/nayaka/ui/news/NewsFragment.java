package com.eisbetech.nayaka.ui.news;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.Berita;
import com.eisbetech.nayaka.ui.home.NewsHomeAdapter;
import com.eisbetech.nayaka.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewsFragment extends Fragment {

    private List<Berita> listBerita;
    private NewsAdapter newsAdapter;

    @BindView(R.id.rvNews)
    RecyclerView recyclerView;

    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);

        Berita berita1 = new Berita("Tips Mengatur Keuangan Rumah Tangga yang Bergaji 5 Juta", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn1-production-images-kly.akamaized.net/yhf54xmfDVI_9s2Wv1PPrbl2Eqc=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2285188/original/080488700_1531990071-Tips_Mengatur_Keuangan.jpg");
        Berita berita2 = new Berita("Re Clean, Solusi Terbaru Mencuci Tanpa Kucek dan Menggiling Pakaian", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn1-production-images-kly.akamaized.net/08Zhak98fptv_VP4Y1A8nZtIORY=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2243280/original/026400700_1528430560-Mengucek.jpg");
        Berita berita3 = new Berita("Pisau Bisa Jadi Penentu Makanan Lezat, Kok Bisa?", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn1-production-images-kly.akamaized.net/kbovJbVX7JkTj7g7pg_9Ju7l8M8=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2044440/original/039698500_1522472186-tokped.jpg");
        Berita berita4 = new Berita("Kesal Diberi Review Jelek, Pemilik Toko Online Hajar Pembeli", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "https://cdn0-production-images-kly.akamaized.net/_2sJQLXp3NLvbZlsj0ntl5hZcFc=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/1596570/original/088231500_1495007197-3.jpg");

        listBerita = new ArrayList<>();
        listBerita.add(berita1);
        listBerita.add(berita2);
        listBerita.add(berita3);
        listBerita.add(berita4);

        newsAdapter = new NewsAdapter(getActivity(), listBerita);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new Constant.LeftItemDecotaion(15));
        recyclerView.setAdapter(newsAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}