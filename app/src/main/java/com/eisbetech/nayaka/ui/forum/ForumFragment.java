package com.eisbetech.nayaka.ui.forum;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.nayaka.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ForumFragment extends Fragment {

    @BindView(R.id.tab_forum)
    TabLayout mTabForum;
    @BindView(R.id.content_container_forum)
    ViewPager mContentContainerForum;

    private ForumTabAdapter mForumAdapter;
    private View view;
    Unbinder unbinder;

    public ForumFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forum, container, false);

        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);

        mForumAdapter = new ForumTabAdapter(getChildFragmentManager());
        mContentContainerForum.setAdapter(mForumAdapter);
        mContentContainerForum.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabForum));
        mTabForum.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mContentContainerForum));

        mContentContainerForum.setOffscreenPageLimit(10);

        return view;
    }

    public class ForumTabAdapter extends FragmentPagerAdapter {

        private Fragment choosed;

        public ForumTabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            choosed = new ContentForumFragment();

            if (position == 0) {
                choosed = new ContentForumFragment();
            } else if (position == 1) {
                choosed = new ContentForumFragment();
            } else if (position == 2) {
                choosed = new ContentForumFragment();
            } else if (position == 3) {
                choosed = new ContentForumFragment();
            } else {
                choosed = new ContentForumFragment();
            }

            return choosed;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
