package com.eisbetech.nayaka.ui.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.ui.login.LoginActivity;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AccountFragment extends Fragment {

    @BindView(R.id.textImage)
    TextView mTextImage;
    @BindView(R.id.fullname)
    TextView mFullname;
    @BindView(R.id.yourEmail)
    TextView mYourEmail;
    @BindView(R.id.phone)
    TextView mPhone;
    @BindView(R.id.btnEdit)
    TextView mBtnEdit;
    @BindView(R.id.logout)
    LinearLayout mLogout;

    private View view;
    private Unbinder unbinder;

    public AccountFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @OnClick({R.id.btnEdit, R.id.logout})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.btnEdit:
                startActivity(new Intent(getContext(), AccountEditActivity.class));
                break;
            case R.id.logout:
                Prefs.clear();
                startActivity(new Intent(getContext(), LoginActivity.class));
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
