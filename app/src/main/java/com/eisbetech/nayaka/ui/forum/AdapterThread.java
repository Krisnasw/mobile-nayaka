package com.eisbetech.nayaka.ui.forum;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.ThreadItem;

import java.text.SimpleDateFormat;
import java.util.List;

public class AdapterThread extends RecyclerView.Adapter<AdapterThread.holder> {
    Context context;
    List<ThreadItem> data;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

    public AdapterThread(Context context, List<ThreadItem> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_forum, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        ThreadItem dat = data.get(position);
        holder.mContentThread.setText(dat.getContent());
        holder.mNamaThreadItem.setText(dat.getName());
        holder.mTanggalThreadItem.setText(dateFormat.format(dat.getTimestamp()));
        Glide.with(context.getApplicationContext()).load(dat.getAvatar()).apply(RequestOptions.circleCropTransform()).into(holder.mAvatarThread);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {

        ImageView mAvatarThread;
        TextView mNamaThreadItem;
        TextView mTanggalThreadItem;
        TextView mContentThread;
        TextView mItemBalas;

        public holder(View itemView) {
            super(itemView);
            mAvatarThread = (ImageView) itemView.findViewById(R.id.avatar_thread);
            mNamaThreadItem = (TextView) itemView.findViewById(R.id.nama_thread_item);
            mTanggalThreadItem = (TextView) itemView.findViewById(R.id.tanggal_thread_item);
            mContentThread = (TextView) itemView.findViewById(R.id.content_thread);
            mItemBalas = (TextView) itemView.findViewById(R.id.item_balas);
        }
    }
}
