package com.eisbetech.nayaka.ui.forum;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.remote.ThreadItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ForumDetailActivity extends AppCompatActivity {

    @BindView(R.id.comment_field)
    LinearLayout mCommentField;
    @BindView(R.id.image_promo)
    ImageView mImagePromo;
    @BindView(R.id.progres_image_promo)
    ProgressBar mProgresImagePromo;
    @BindView(R.id.title_thread)
    TextView mTitleThread;
    @BindView(R.id.content_thread)
    TextView mContentThread;
    @BindView(R.id.list_thread)
    RecyclerView mListThread;

    private List<ThreadItem> dataThread;
    private AdapterThread mThreadAdapter;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_detail);

        ButterKnife.bind(this);
        unbinder = ButterKnife.bind(this);

        getSupportActionBar().setTitle("Thread");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Glide.with(getApplicationContext()).load("http://zacharys.com/wp-content/uploads/2014/04/PizzaThinTomBasil_12.jpg").listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                mProgresImagePromo.setVisibility(View.GONE);
                return false;
            }
        }).into(mImagePromo);

        dataThread = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ThreadItem forum1 = new ThreadItem("Adi", System.currentTimeMillis(), "Lorem ipsum dolor sit amet,ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&h=350");
            ThreadItem forum2 = new ThreadItem("Supri", System.currentTimeMillis(), "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu", "https://cap.stanford.edu/profiles/viewImage?profileId=19141&type=square&ts=1509532892453");
            ThreadItem forum3 = new ThreadItem("Rani", System.currentTimeMillis(), "nsectetur adipiscing elit, sed do eiusmod tempor incididunt ut ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&h=350");
            ThreadItem forum4 = new ThreadItem("Ed sheran", System.currentTimeMillis(), "ut labore et dolore magna aliqua. ", "http://www.lionelindia.com/mice/img/user3.jpg");
            dataThread.add(forum1);
            dataThread.add(forum2);
            dataThread.add(forum3);
            dataThread.add(forum4);
        }

        mThreadAdapter = new AdapterThread(this, dataThread);
        mListThread.setLayoutManager(new LinearLayoutManager(this));
        mListThread.setAdapter(mThreadAdapter);
        mListThread.setNestedScrollingEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
