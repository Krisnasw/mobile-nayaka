package com.eisbetech.nayaka.ui.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.nayaka.R;
import com.eisbetech.nayaka.data.local.GridModel;

import java.util.List;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.MyViewHolder> {
    Context context;
    private List<GridModel> OfferList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView fieldImage;
        TextView fieldName;
        CardView tick;

        public MyViewHolder(View view) {
            super(view);
            fieldName = (TextView) view.findViewById(R.id.fieldName);
            fieldImage = (ImageView) view.findViewById(R.id.fieldImage);
            tick = (CardView) view.findViewById(R.id.tick);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (OfferList.get(getAdapterPosition()).getFieldName().toLowerCase().equals("pulsa")){

                    }
                }
            });
        }
    }

    public AdapterHome(Context mainActivityContacts, List<GridModel> offerList) {
        this.OfferList = offerList;
        this.context = mainActivityContacts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grid, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GridModel lists = OfferList.get(position);
        holder.fieldName.setText(lists.getFieldName());
        holder.fieldImage.setImageResource(lists.getFieldImage());
    }

    @Override
    public int getItemCount() {
        return OfferList.size();
    }
}
