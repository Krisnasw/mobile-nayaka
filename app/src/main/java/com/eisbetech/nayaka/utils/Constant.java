package com.eisbetech.nayaka.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.text.NumberFormat;
import java.util.Locale;

public class Constant {

    public static String toRupiah(long value){
        Locale locale = new Locale("in","ID");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

        return numberFormat.format(value).replace("Rp","Rp. ");
    }

    public static String toTimeFormat(long start,long end){
        long lama = Math.abs((end - start));

        return  ((lama/60)>0?lama/60+"j ":"")+((lama%60)>0?lama%60+"m":"")+"";
    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    public static class EdgeDecorator extends RecyclerView.ItemDecoration {

        private final int edgePadding;

        /**
         * EdgeDecorator
         * @param edgePadding padding set on the left side of the first item and the right side of the last item
         */
        public EdgeDecorator(int edgePadding) {
            this.edgePadding = edgePadding;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            int itemCount = state.getItemCount();

            final int itemPosition = parent.getChildAdapterPosition(view);

            // no position, leave it alone
            if (itemPosition == RecyclerView.NO_POSITION) {
                return;
            }

            // first item
            if (itemPosition == 0 || itemPosition == 1) {
                outRect.set(edgePadding, 0, 0, 0);
            }
            // every other item
            else {
                outRect.set(0, 0, 0, 0);
            }
        }
    }

    public static class LeftItemDecotaion extends RecyclerView.ItemDecoration {
        private int spacing;

        public LeftItemDecotaion(int spacing) {
            this.spacing = spacing;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            if (position==0) {
                outRect.left = spacing;
            }
        }
    }

}
